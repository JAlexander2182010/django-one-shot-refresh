from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todo_list/todo_list.html", context)